/*S1

        Class A{}

        Class B extends A{

public List<C> cList;
        cList=new ArrayList<>();
private String param;
void metz( Z z)

        }

        Class C{}

        Class D{
public B b=new B();
public void f();

        }

        Class E{
public List<B> bList;
public E(List<B> bList){

        this.bList=bList;
        }
        }

        Class Z{
public void g();

        }*/

//S2

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;

public class ButtonAndTextField extends JFrame {

    HashMap texts = new HashMap();

    JLabel text;
    JTextField tText;
    JButton button;
    String constant = "name.txt";

    ButtonAndTextField() {

        setTitle("Exam");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200, 300);
        setVisible(true);
    }

    public void init() {

        this.setLayout(null);
        int width = 80;
        int height = 20;

        text = new JLabel("Text");
        text.setBounds(10, 50, width, height);

        tText = new JTextField();
        tText.setBounds(70, 50, width, height);

        button = new JButton("Button");
        button.setBounds(10, 150, width, height);

        button.addActionListener(new TratareButon());

        add(text);
        add(tText);
        add(button);

    }

    public static void main(String[] args) {
        new ButtonAndTextField();
    }

    class TratareButon implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            String text = ButtonAndTextField.this.tText.getText();

            if (ButtonAndTextField.this.texts.containsKey(text)) {

                try {

                    Files.writeString(Paths.get(constant), text,
                            StandardCharsets.ISO_8859_1,
                            StandardOpenOption.CREATE,
                            StandardOpenOption.APPEND);

                } catch (IOException ex) {
                    ex.printStackTrace();

                }
            }

        }
    }
}
